-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.21-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for edatablockrpamain
CREATE DATABASE IF NOT EXISTS `edatablockrpamain` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `edatablockrpamain`;

-- Dumping structure for table edatablockrpamain.client
CREATE TABLE IF NOT EXISTS `client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `client_address` varchar(255) DEFAULT NULL,
  `client_email_address` varchar(255) NOT NULL,
  `is_active` int(11) DEFAULT NULL,
  `create_date` datetime,
  `created_by` varchar(255) DEFAULT NULL,
  `update_date` datetime,
  `is_merged_document` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `org_name_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_client_org_name_id` (`org_name_id`),
  CONSTRAINT `fk_client_org_name_id` FOREIGN KEY (`org_name_id`) REFERENCES `organization` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table edatablockrpamain.client: ~2 rows (approximately)
DELETE FROM `client`;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` (`id`, `client_name`, `description`, `client_address`, `client_email_address`, `is_active`, `create_date`, `created_by`, `update_date`, `is_merged_document`, `updated_by`, `org_name_id`) VALUES
	(1, 'Airfrance', '', '', 'mail2fr@airfrance.fr', 1, NULL, '', NULL, NULL, '', 1),
	(2, 'dbshenker', '', '', 'abc@dbschenker.com', NULL, NULL, '', NULL, NULL, '', 1),
	(3, 'dbschenker', '', 'xyz', 'xyz@dbschenker.com', NULL, NULL, '', NULL, NULL, '', 1);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
