-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.21-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for edatablockrpamain
CREATE DATABASE IF NOT EXISTS `edatablockrpamain` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `edatablockrpamain`;

-- Dumping structure for table edatablockrpamain.input_template
CREATE TABLE IF NOT EXISTS `input_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) NOT NULL,
  `template_description` varchar(255) DEFAULT NULL,
  `template_type` varchar(255) NOT NULL,
  `is_standard_template` bit(1) NOT NULL,
  `is_active` int(11) DEFAULT NULL,
  `create_date` datetime,
  `created_by` varchar(255) DEFAULT NULL,
  `update_date` datetime,
  `template_identifier` varchar(255) DEFAULT NULL,
  `number_of_pages` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_input_template_client_id` (`client_id`),
  CONSTRAINT `fk_input_template_client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table edatablockrpamain.input_template: ~6 rows (approximately)
DELETE FROM `input_template`;
/*!40000 ALTER TABLE `input_template` DISABLE KEYS */;
INSERT INTO `input_template` (`id`, `template_name`, `template_description`, `template_type`, `is_standard_template`, `is_active`, `create_date`, `created_by`, `update_date`, `template_identifier`, `number_of_pages`, `updated_by`, `client_id`) VALUES
	(1, 'AWB', '', 'AWB', b'1', 1, NULL, '', NULL, 'A', 1, '', 1),
	(2, 'INV', '', 'INV', b'1', 1, NULL, '', NULL, '', NULL, '', 1),
	(3, 'TEMPLATE3', '', 'AWB', b'1', 1, NULL, '', NULL, '', NULL, '', 2),
	(4, 'TEMPLATE4', '', 'AWB', b'1', 1, NULL, '', NULL, '', NULL, '', 2),
	(5, 'TEMPLATE5', '', 'AWB', b'1', 1, NULL, '', NULL, '', NULL, '', 3),
	(6, 'TEMPLATE6', '', 'AWB', b'1', NULL, NULL, '', NULL, '', NULL, '', 3);
/*!40000 ALTER TABLE `input_template` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
