-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.21-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for edatablockrpamain
CREATE DATABASE IF NOT EXISTS `edatablockrpamain` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `edatablockrpamain`;

-- Dumping structure for table edatablockrpamain.organization
CREATE TABLE IF NOT EXISTS `organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `org_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `org_address` varchar(255) DEFAULT NULL,
  `org_email` varchar(255) NOT NULL,
  `is_active` int(11) DEFAULT NULL,
  `create_date` datetime,
  `created_by` varchar(255) DEFAULT NULL,
  `update_date` datetime,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table edatablockrpamain.organization: ~1 rows (approximately)
DELETE FROM `organization`;
/*!40000 ALTER TABLE `organization` DISABLE KEYS */;
INSERT INTO `organization` (`id`, `org_name`, `description`, `org_address`, `org_email`, `is_active`, `create_date`, `created_by`, `update_date`, `updated_by`) VALUES
	(1, 'CGL', 'CGL', '', 'abc.cgl.com', 1, NULL, '', NULL, '');
/*!40000 ALTER TABLE `organization` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
