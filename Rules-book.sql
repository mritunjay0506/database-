-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.21-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for edatablockrpamain
CREATE DATABASE IF NOT EXISTS `edatablockrpamain` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `edatablockrpamain`;

-- Dumping structure for table edatablockrpamain.rules_book
CREATE TABLE IF NOT EXISTS `rules_book` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rule_number` varchar(255) DEFAULT NULL,
  `rule_sequence` int(11) DEFAULT NULL,
  `jhi_key` int(11) DEFAULT NULL,
  `lookup_place_1` varchar(255) DEFAULT NULL,
  `operator_1` varchar(255) DEFAULT NULL,
  `value_1` varchar(255) DEFAULT NULL,
  `join_field_1` varchar(255) DEFAULT NULL,
  `lookup_place_2` varchar(255) DEFAULT NULL,
  `operator_2` varchar(255) DEFAULT NULL,
  `value_2` varchar(255) DEFAULT NULL,
  `join_field_2` varchar(255) DEFAULT NULL,
  `lookup_place_3` varchar(255) DEFAULT NULL,
  `operator_3` varchar(255) DEFAULT NULL,
  `value_3` varchar(255) DEFAULT NULL,
  `result` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table edatablockrpamain.rules_book: ~12 rows (approximately)
DELETE FROM `rules_book`;
/*!40000 ALTER TABLE `rules_book` DISABLE KEYS */;
INSERT INTO `rules_book` (`id`, `rule_number`, `rule_sequence`, `jhi_key`, `lookup_place_1`, `operator_1`, `value_1`, `join_field_1`, `lookup_place_2`, `operator_2`, `value_2`, `join_field_2`, `lookup_place_3`, `operator_3`, `value_3`, `result`, `description`) VALUES
	(1, '00001', 1, NULL, 'EMAIL_BODY', 'CONTAINS', 'mail2fr@airfrance.fr', '', '', '', '', '', '', '', '', 1, ''),
	(2, '00001', 2, NULL, 'EMAIL_BODY', 'CONTAINS', '@dbschenker.com', 'AND', 'SUBJECT', 'CONTAINS', 'ABC', '', '', '', '', 2, ''),
	(3, '00001', 3, NULL, 'EMAIL_BODY', 'CONTAINS', '@dbschenker.com', 'AND', 'SUBJECT', 'CONTAINS', 'XYZ', '', '', '', '', 3, ''),
	(4, '00001', 4, NULL, 'SUBJECT', 'CONTAINS', 'PQR', 'OR', 'MAIL_BODY', 'CONTAINS', 'ABC@ABC.COM', '', '', '', '', 4, ''),
	(5, '00002', 1, 1, 'FILE_NAME', 'START_WITH', 'AWB', '', '', '', '', '', '', '', '', 1, ''),
	(6, '00002', 2, 1, 'FILE_NAME', 'START_WITH', 'INV', '', '', '', '', '', '', '', '', 2, ''),
	(7, '00002', 3, 2, 'SUBJECT', 'CONTAINS', 'ASDFG', '', '', '', '', '', '', '', '', 4, ''),
	(8, '00002', 4, 3, 'FILE_NAME', 'START_WITH', 'AW', '', '', '', '', '', '', '', '', 3, ''),
	(9, '00002', 5, 3, 'FILE_NAME', 'START_WITH', 'IN', '', '', '', '', '', '', '', '', 3, ''),
	(10, '00001', 5, 5, 'SENDER', 'EQUALS', 'Manish.kr05@gmail.com', '', '', '', '', '', '', '', '', 5, ''),
	(11, '00002', 6, 5, 'FILE_NAME', 'START_WITH', 'AWB', '', '', '', '', '', '', '', '', 5, ''),
	(12, '00002', 7, 5, 'FILE_NAME', 'START_WITH', 'INV', '', '', '', '', '', '', '', '', 5, '');
/*!40000 ALTER TABLE `rules_book` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
