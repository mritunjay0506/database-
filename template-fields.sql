-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.21-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for edatablockrpamain
CREATE DATABASE IF NOT EXISTS `edatablockrpamain` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `edatablockrpamain`;

-- Dumping structure for table edatablockrpamain.template_fields
CREATE TABLE IF NOT EXISTS `template_fields` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(255) NOT NULL,
  `field_zone_min_x` double NOT NULL,
  `field_zone_min_y` double NOT NULL,
  `field_zone_max_x` double NOT NULL,
  `field_zone_max_y` double NOT NULL,
  `width` double DEFAULT NULL,
  `height` double DEFAULT NULL,
  `jhi_sequence` int(11) DEFAULT NULL,
  `is_template_identifier` int(11) DEFAULT NULL,
  `is_label` int(11) DEFAULT NULL,
  `page_numebr` int(11) DEFAULT NULL,
  `field_validation_require` int(11) DEFAULT NULL,
  `field_validation_rule` varchar(255) DEFAULT NULL,
  `input_template_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_template_fields_input_template_id` (`input_template_id`),
  CONSTRAINT `fk_template_fields_input_template_id` FOREIGN KEY (`input_template_id`) REFERENCES `input_template` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table edatablockrpamain.template_fields: ~8 rows (approximately)
DELETE FROM `template_fields`;
/*!40000 ALTER TABLE `template_fields` DISABLE KEYS */;
INSERT INTO `template_fields` (`id`, `field_name`, `field_zone_min_x`, `field_zone_min_y`, `field_zone_max_x`, `field_zone_max_y`, `width`, `height`, `jhi_sequence`, `is_template_identifier`, `is_label`, `page_numebr`, `field_validation_require`, `field_validation_rule`, `input_template_id`) VALUES
	(1, 'Supplier', 650, 1100, 110, 1100, 1, 1, 1, NULL, NULL, 1, NULL, '', 1),
	(2, 'b', 2, 2, 2, 2, 2, 2, 2, NULL, NULL, NULL, NULL, '', 6),
	(3, 'c', 3, 3, 3, 3, 3, 3, 3, NULL, NULL, NULL, NULL, '', 3),
	(4, 'd', 4, 4, 4, 4, 4, 4, 4, NULL, NULL, NULL, NULL, '', 4),
	(5, 'e', 5, 5, 5, 5, 5, 5, 5, NULL, NULL, NULL, NULL, '', 5),
	(6, 'Supplier', 650, 1100, 110, 1100, 6, 6, 1, NULL, NULL, NULL, NULL, '', 2),
	(7, 'AWB NO', 650, 1100, 110, 1100, NULL, NULL, 2, NULL, NULL, 1, NULL, '', 1),
	(8, 'AWB NO', 650, 1100, 110, 1100, NULL, NULL, 2, NULL, NULL, 1, NULL, '', 2);
/*!40000 ALTER TABLE `template_fields` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
